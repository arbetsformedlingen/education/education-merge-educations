import logging
import os

from educationmerge import settings
from educationmerge.minio_downloader import MinioDownloader
from educationmerge.minio_uploader import MinioUploader

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

educations_output_file_prefix = settings.MERGE_EDUCATIONS_S3_EDUCATIONS_DEST_FILE_PREFIX

# Internal structure with education_code as keys and array of educations as values:
educations_with_plans = {}
# Internal structures for logging statistics:
# These holds the whole data structures for ease debugging:
stat_merged_programs = []
stat_merged_courses = []
stat_educations_without_code = []

# These holds number of occurrences:
global stat_number_of_educations_from_input
global stat_number_of_education_plans_from_input


def _merge_education_with_plan(education_plan, EDUCATION_TYPE):
    if EDUCATION_TYPE != 'course_code' and EDUCATION_TYPE != 'program_code':
        log.warning(f'Unknown code type {EDUCATION_TYPE}')

    if education_plan and education_plan.get(EDUCATION_TYPE):
        course_code = education_plan[EDUCATION_TYPE]
        if educations_with_plans.get(course_code):
            merged_educations = educations_with_plans[course_code]
            for merged_education in merged_educations:
                merged_education['education_plan'] = education_plan
                if EDUCATION_TYPE == 'course_code':
                    stat_merged_courses.append(merged_education)
                elif EDUCATION_TYPE == 'program_code':
                    stat_merged_programs.append(merged_education)
            educations_with_plans[course_code] = merged_educations


def merge_educations():
    log.info('Starting to merge educations and educationplans..')
    stat_number_of_educations_from_input = 0
    stat_number_of_education_plans_from_input = 0

    minio_downloader = MinioDownloader()
    minio_uploader = MinioUploader()

    # Fetch educations from files on minio...
    for education in minio_downloader.download_educations():
        stat_number_of_educations_from_input += 1

        if education and education.get('education', None):
            education_dict = education['education']
            if education_dict.get('code', None):
                education_code = education_dict['code']
            else:
                education_code = '-1'
                stat_educations_without_code.append(education)
            if educations_with_plans.get(education_code):
                education_list = educations_with_plans[education_code]
                education_list.append(education)
            else:
                education_list = []
                education_list.append(education)
            educations_with_plans[education_code] = education_list
        else:
            log.warning(f'The education was empty (could not be parsed), skipping')

    # Fetch education plans from files on minio and merge to matching educations with code as key...
    for education_plan in minio_downloader.download_education_plans():
        stat_number_of_education_plans_from_input += 1
        _merge_education_with_plan(education_plan, 'program_code')
        _merge_education_with_plan(education_plan, 'course_code')

    # Convert educations dict to list...
    educations_to_upload = []
    for education_code, educations in educations_with_plans.items():
        educations_to_upload.extend(educations)

    if educations_to_upload and len(educations_to_upload) > 0:
        for education in educations_to_upload:
            # Add common education providers fields to root of education dict to be easily accessible
            # (i.e. without having to parse the whole provider structure)...
            education['providerSummary'] = extractCommonFieldsFromProviders(education)
            # Add common education events fields to root of education dict to be easily accessible
            # (i.e. without having to parse the whole event structure)...
            education['eventSummary'] = extractCommonFieldsFromEvents(education)

    # Upload merged file to minio...
    if educations_to_upload and len(educations_to_upload) > 0:
        from datetime import datetime
        time_stamp = datetime.now().strftime("%Y-%m-%d_%I_%M_%S")
        file_name_dest = f'{educations_output_file_prefix}{time_stamp}.jsonl'
        minio_uploader.upload_json_data(educations_to_upload, file_name_dest)
    log.info(
        f'Finish merge educations and plans. Number of educations from input: {stat_number_of_educations_from_input}. {os.linesep}' 
        f'Number of education plans from input: {stat_number_of_education_plans_from_input}. {os.linesep}'
        f'Number of educations without code: {len(stat_educations_without_code)}. {os.linesep}'
        f'Number of programs merged with education plan: {len(stat_merged_programs)}. {os.linesep}'
        f'Number of courses merged with course plan: {len(stat_merged_courses)}. {os.linesep}'
        f'Number of educations to output: {len(educations_to_upload)}.')
    if len(educations_to_upload) != stat_number_of_educations_from_input:
        log.warning(f'Not same number of educations from input as to output, i.e. '
                    f'{stat_number_of_educations_from_input} vs {len(educations_to_upload)}')


def extractCommonFieldsFromProviders(education):
    providers = set()
    # Extract significant provider fields to be accessible in education part:
    if education.get('education_providers'):
        for education_provider in education['education_providers']:
            if education_provider.get('name'):
                for name in education_provider['name']:
                    if name.get('lang') and name['lang'] == 'swe' and name.get('content'):
                        providers.add(name['content'])

    extractedProvidersFields = {'providers': list(providers)}
    return extractedProvidersFields


def extractCommonFieldsFromEvents(education):
    municipalityCode = set()
    regionCode = set()
    languageOfInstruction = set()
    onlyAsPartOfProgram = set()
    paceOfStudyPercentage = set()
    timeOfStudy = set()
    tuitionFee = set()
    distance = False
    executionStartEndList = list()
    # Extract significant event fields to be accessible in education part:
    if education.get('events'):
        for event in education['events']:
            if event.get('languageOfInstruction'):
                languageOfInstruction.add(event['languageOfInstruction'])

            if event.get('applicationDetails'):
                event_application = event['applicationDetails']
                if event_application.get('onlyAsPartOfProgram'):
                    onlyAsPartOfProgram.add(event_application['onlyAsPartOfProgram'])

            if event.get('paceOfStudyPercentage'):
                paceOfStudyPercentage.add(event['paceOfStudyPercentage'])

            if event.get('timeOfStudy'):
                event_time_of_study = event['timeOfStudy']
                if event_time_of_study.get('code'):
                    timeOfStudy.add(event_time_of_study['code'])

            if event.get('tuitionFee'):
                event_tuitionfee = event['tuitionFee']
                if event_tuitionfee.get('total'):
                    tuitionFee.add(event_tuitionfee['total'])

            if event.get('locations'):
                event_locations = event['locations']
                for event_location in event_locations:
                    if (event_location.get('country') and event_location['country'] == 'SE'):
                        if (event_location.get('municipalityCode')):
                            municipalityCode.add(event_location['municipalityCode'])
                        if (event_location.get('regionCode')):
                            regionCode.add(event_location['regionCode'])

            if event.get('distance'):
                event_distance = event['distance']
                if event_distance.get('mandatory') or event_distance.get('optional') or event_distance.get('mandatoryNet') or event_distance.get('optionalNet'):
                    distance = True

            if event.get('execution'):
                # Add unique start and end dates to the event summary
                event_execution = event['execution']
                start_value = event_execution.get('start')
                end_value = event_execution.get('end')
                start_end_value = {'start': start_value, 'end': end_value}
                if start_end_value not in executionStartEndList:
                    executionStartEndList.append(start_end_value)

    extractedEventsFields = {'municipalityCode': list(municipalityCode),
                             'regionCode': list(regionCode),
                             'languageOfInstruction': list(languageOfInstruction),
                             'onlyAsPartOfProgram': list(onlyAsPartOfProgram),
                             'paceOfStudyPercentage': list(paceOfStudyPercentage),
                             'timeOfStudy': list(timeOfStudy),
                             'executions': executionStartEndList,
                             'tuitionFee': list(tuitionFee),
                             'distance': distance}
    return extractedEventsFields
