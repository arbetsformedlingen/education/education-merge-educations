import logging
import os
import sys
import threading
import tempfile
import json

import boto3
from boto3.s3.transfer import S3Transfer
from botocore.client import Config

from educationmerge import settings


class MinioUploader(object):
    s3_url = settings.S3_URL
    s3_bucket_name = settings.S3_BUCKET_NAME
    aws_access_key = settings.AWS_ACCESS_KEY
    aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)

        self.s3_client = boto3.client('s3',
                                      endpoint_url=self.s3_url,
                                      aws_access_key_id=self.aws_access_key,
                                      aws_secret_access_key=self.aws_secret_access_key,
                                      config=Config(signature_version='s3v4'),
                                      region_name='us-east-1')

    def upload_json_data(self, json_data, filename_dest):

        with tempfile.NamedTemporaryFile(mode="w", delete=False) as temp_educations_file:
            filename_source = temp_educations_file.name
            self.log.info(f'Writing output to tempfile {filename_source} before saving in bucket: {self.s3_bucket_name} on minio: {self.s3_url}')
            for record in json_data:
                temp_educations_file.write((json.dumps(record, ensure_ascii=True)).strip() + '\n')

            self.log.info('Uploading file from %s to minio-bucket %s with filename: %s' % (
                filename_source, self.s3_bucket_name, filename_dest))
            transfer = S3Transfer(self.s3_client)
            transfer.upload_file(filename_source, self.s3_bucket_name, filename_dest,
                                 callback=ProgressPercentage(filename_source))


class ProgressPercentage(object):
    def __init__(self, filename):
        self._filename = filename
        self._size = float(os.path.getsize(filename))
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        # To simplify we'll assume this is hooked up
        # to a single filename.
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\r%s  %s / %s  (%.2f%%)" % (
                    self._filename, self._seen_so_far, self._size,
                    percentage))
            sys.stdout.flush()

