import os

VERSION = '0.0.1'

S3_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME', 'kll-test')
AWS_ACCESS_KEY = os.environ.get('AWS_ACCESS_KEY', 'kll')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY', 'abcd1234')
S3_URL = os.environ.get('S3_URL', 'http://localhost:19000')
MERGE_EDUCATIONS_S3_EDUCATIONS_SRC_FILE_PREFIX = os.getenv('MERGE_EDUCATIONS_S3_EDUCATIONS_SRC_FILE_PREFIX', 'educations_')
MERGE_EDUCATIONS_S3_SRC_FILE_MAX_VALID_HOURS = os.getenv('MERGE_EDUCATIONS_S3_SRC_FILE_MAX_VALID_HOURS', '24')
MERGE_EDUCATIONS_S3_EDUCATIONPLANS_SRC_FILE_PREFIX = os.getenv('MERGE_EDUCATIONS_S3_EDUCATIONPLANS_SRC_FILE_PREFIX', 'educationplans_')
MERGE_EDUCATIONS_S3_EDUCATIONS_DEST_FILE_PREFIX = os.getenv('MERGE_EDUCATIONS_S3_EDUCATIONS_DEST_FILE_PREFIX', 'merged_educations_')